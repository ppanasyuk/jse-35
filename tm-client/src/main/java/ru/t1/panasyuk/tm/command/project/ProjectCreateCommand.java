package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Create new project.";

    @NotNull
    private static final String NAME = "project-create";

    @Override
    public void execute() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        getProjectEndpoint().createProject(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}