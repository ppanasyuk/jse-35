package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-login";

    @NotNull
    private final String DESCRIPTION = "User login.";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        @Nullable final String token = getAuthEndpoint().loginUser(request).getToken();
        setToken(token);
        System.out.println(token);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}