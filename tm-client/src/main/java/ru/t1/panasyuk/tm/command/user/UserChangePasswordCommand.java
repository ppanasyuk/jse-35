package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.user.UserChangePasswordRequest;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-change-password";

    @NotNull
    private final String DESCRIPTION = "Change password of current user.";

    @Override
    public void execute() {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken(), password);
        getUserEndpoint().changeUserPassword(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}