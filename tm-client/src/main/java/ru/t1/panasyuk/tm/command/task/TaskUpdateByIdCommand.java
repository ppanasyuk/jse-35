package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.task.TaskUpdateByIdRequest;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Update task by id.";

    @NotNull
    private static final String NAME = "task-update-by-id";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(getToken(), id, name, description);
        getTaskEndpoint().updateTaskById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}