package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.project.ProjectCompleteByIndexRequest;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    private static final String NAME = "project-complete-by-index";

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken(), index);
        getProjectEndpoint().completeProjectByIndex(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}