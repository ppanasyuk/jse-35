package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.PropertyService;
import ru.t1.panasyuk.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @Before
    public void initRepository() {
        userList = new ArrayList<>();
        userRepository = new UserRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("USER" + i);
            user.setEmail("USER@" + i);
            user.setFirstName("User" + i);
            user.setLastName("User LastName" + i);
            user.setPasswordHash(HashUtil.salt(new PropertyService(), "qwerty" + i));
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void addTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final String userFirstName = "User FirstName";
        @NotNull final String userLastName = "User LastName";
        @NotNull final String userLogin = "User Login";
        @NotNull final String userEmail = "User Description";
        @NotNull final User user = new User();
        user.setFirstName(userFirstName);
        user.setLastName(userLastName);
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        @Nullable final User createdUser = userRepository.findOneByIndex(userRepository.getSize() - 1);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(userFirstName, createdUser.getFirstName());
        Assert.assertEquals(userLastName, createdUser.getLastName());
        Assert.assertEquals(userLogin, createdUser.getLogin());
        Assert.assertEquals(userEmail, createdUser.getEmail());
    }

    @Test
    public void addAllTest() {
        int expectedNumberOfEntries = userRepository.getSize() + 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "User 1 FirstName";
        @NotNull final String firstUserLastName = "User 1 LastName";
        @NotNull final String firstUserLogin = "User 1 Login";
        @NotNull final String firstUserEmail = "User 1 Description";
        @NotNull final User firstUser = new User();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "User 2 FirstName";
        @NotNull final String secondUserLastName = "User 2 LastName";
        @NotNull final String secondUserLogin = "User 2 Login";
        @NotNull final String secondUserEmail = "User 2 Description";
        @NotNull final User secondUser = new User();
        firstUser.setFirstName(secondUserFirstName);
        firstUser.setLastName(secondUserLastName);
        firstUser.setLogin(secondUserLogin);
        firstUser.setEmail(secondUserEmail);
        users.add(secondUser);
        @NotNull final Collection<User> addedUsers = userRepository.add(users);
        Assert.assertTrue(addedUsers.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        userRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void existByIdTrueTest() {
        boolean expectedResult = true;
        @Nullable final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        @Nullable final String userId = user.getId();
        Assert.assertEquals(expectedResult, userRepository.existsById(userId));
    }

    @Test
    public void existByIdFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, userRepository.existsById("qwerty"));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<User> users = userRepository.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void findByLoginTest() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            @Nullable final User foundUser = userRepository.findByLogin(login);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void findByLoginNullTest() {
        @Nullable final User foundUserNull = userRepository.findByLogin(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByLogin("123321");
        Assert.assertNull(foundUser);
    }

    @Test
    public void findByEmailTest() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            @Nullable final User foundUser = userRepository.findByEmail(email);
            Assert.assertEquals(user, foundUser);
        }
    }

    @Test
    public void findByEmailNullTest() {
        @Nullable final User foundUserNull = userRepository.findByEmail(null);
        Assert.assertNull(foundUserNull);
        @Nullable final User foundUser = userRepository.findByEmail("123321");
        Assert.assertNull(foundUser);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable User user;
        for (int i = 0; i < userList.size(); i++) {
            user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User foundUser = userRepository.findOneById(userId);
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final User foundUser = userRepository.findOneById("qwerty");
        Assert.assertNull(foundUser);
        @Nullable final User foundUserNull = userRepository.findOneById(null);
        Assert.assertNull(foundUserNull);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < userList.size(); i++) {
            @Nullable final User user = userRepository.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test
    public void findOneByIndexNullTest() {
        @Nullable final User user = userRepository.findOneByIndex(null);
        Assert.assertNull(user);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = userList.size();
        int actualSize = userRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void isEmailExistTest() {
        for (@NotNull final User user : userList) {
            @Nullable final String email = user.getEmail();
            final boolean isEmailExists = userRepository.isEmailExist(email);
            Assert.assertTrue(isEmailExists);
        }
    }

    @Test
    public void isEmailExistNullTest() {
        final boolean isEmailExistsNull = userRepository.isEmailExist(null);
        Assert.assertFalse(isEmailExistsNull);
        final boolean isEmailExists = userRepository.isEmailExist("123321");
        Assert.assertFalse(isEmailExists);
    }

    @Test
    public void isLoginExistTest() {
        for (@NotNull final User user : userList) {
            @Nullable final String login = user.getLogin();
            final boolean isLoginExists = userRepository.isLoginExist(login);
            Assert.assertTrue(isLoginExists);
        }
    }

    @Test
    public void isLoginExistsNull() {
        final boolean isLoginExistsNull = userRepository.isLoginExist(null);
        Assert.assertFalse(isLoginExistsNull);
        final boolean isLoginExists = userRepository.isLoginExist("123321");
        Assert.assertFalse(isLoginExists);
    }

    @Test
    public void removeAllNullTest() {
        int expectedNumberOfEntries = userRepository.getSize();
        userRepository.removeAll(null);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void removeTest() {
        @Nullable final User user = userRepository.findOneByIndex(0);
        Assert.assertNotNull(user);
        @NotNull final String userId = user.getId();
        @Nullable final User deletedUser = userRepository.remove(user);
        Assert.assertNotNull(deletedUser);
        @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
        Assert.assertNull(deletedUserInRepository);
    }

    @Test
    public void removeNullTest() {
        @Nullable final User user = userRepository.remove(null);
        Assert.assertNull(user);
    }

    @Test
    public void removeByIdTest() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User user = userRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(user);
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userRepository.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullTest() {
        @Nullable final User deletedUser = userRepository.removeById("qwerty");
        Assert.assertNull(deletedUser);
        @Nullable final User deletedUserNull = userRepository.removeById(null);
        Assert.assertNull(deletedUserNull);
    }

    @Test
    public void removeByIndexTest() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User deletedUser = userRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedUser);
            @NotNull final String userId = deletedUser.getId();
            @Nullable final User deletedUserInRepository = userRepository.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<User> users = new ArrayList<>();
        @NotNull final String firstUserFirstName = "User 1 FirstName";
        @NotNull final String firstUserLastName = "User 1 LastName";
        @NotNull final String firstUserLogin = "User 1 Login";
        @NotNull final String firstUserEmail = "User 1 Description";
        @NotNull final User firstUser = new User();
        firstUser.setFirstName(firstUserFirstName);
        firstUser.setLastName(firstUserLastName);
        firstUser.setLogin(firstUserLogin);
        firstUser.setEmail(firstUserEmail);
        users.add(firstUser);
        @NotNull final String secondUserFirstName = "User 2 FirstName";
        @NotNull final String secondUserLastName = "User 2 LastName";
        @NotNull final String secondUserLogin = "User 2 Login";
        @NotNull final String secondUserEmail = "User 2 Description";
        @NotNull final User secondUser = new User();
        firstUser.setFirstName(secondUserFirstName);
        firstUser.setLastName(secondUserLastName);
        firstUser.setLogin(secondUserLogin);
        firstUser.setEmail(secondUserEmail);
        users.add(secondUser);
        @NotNull final Collection<User> addedUsers = userRepository.set(users);
        Assert.assertTrue(addedUsers.size() > 0);
        int actualNumberOfEntries = userRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}