package ru.t1.panasyuk.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.field.LoginEmptyException;
import ru.t1.panasyuk.tm.exception.field.PasswordEmptyException;
import ru.t1.panasyuk.tm.exception.system.AccessDeniedException;
import ru.t1.panasyuk.tm.exception.system.AuthenticationException;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;
import ru.t1.panasyuk.tm.util.CryptUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AuthServiceTest {

    @NotNull
    private IAuthService authService;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private IUserService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<User> userList;

    @NotNull
    private User user;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        propertyService = new PropertyService();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(propertyService, userRepository, projectTaskService);
        sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final User test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        user = userService.create("USER", "USER", Role.ADMIN);
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
        @NotNull final Session session1 = new Session();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        @NotNull final Session session2 = new Session();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        @NotNull final Session session3 = new Session();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionService.add(session1);
        sessionService.add(session2);
        sessionService.add(session3);
    }

    @Test
    public void checkTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final User testUser = authService.check(user.getLogin(), user.getLogin());
            Assert.assertNotNull(testUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void checkLoginEmptyTestNegative() {
        @Nullable final User testUser = authService.check("", user.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    public void checkLoginNullTestNegative() {
        @Nullable final User testUser = authService.check(null, user.getLogin());
    }

    @Test(expected = PasswordEmptyException.class)
    public void checkPasswordEmptyTestNegative() {
        @Nullable final User testUser = authService.check(user.getLogin(), "");
    }

    @Test(expected = PasswordEmptyException.class)
    public void checkPasswordNullTestNegative() {
        @Nullable final User testUser = authService.check(user.getLogin(), null);
    }

    @Test(expected = AuthenticationException.class)
    public void checkUserNotFoundTestNegative() {
        @Nullable final User testUser = authService.check("ABYRVALG", "PASS");
    }

    @Test(expected = AuthenticationException.class)
    public void checkUserLockedTestNegative() {
        user.setLocked(true);
        @Nullable final User testUser = authService.check(user.getLogin(), user.getLogin());
    }

    @Test(expected = AuthenticationException.class)
    public void checkWrongPasswordTestNegative() {
        @Nullable final User testUser = authService.check(user.getLogin(), "PASS");
    }

    @Test
    public void loginTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void loginLoginEmptyTestNegative() {
        @Nullable final String token = authService.login("", user.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    public void loginLoginNullTestNegative() {
        @Nullable final String token = authService.login(null, user.getLogin());
    }

    @Test(expected = PasswordEmptyException.class)
    public void loginPasswordEmptyTestNegative() {
        @Nullable final String token = authService.login(user.getLogin(), "");
    }

    @Test(expected = PasswordEmptyException.class)
    public void loginPasswordNullTestNegative() {
        @Nullable final String token = authService.login(user.getLogin(), null);
    }

    @Test(expected = AuthenticationException.class)
    public void loginUserNotFoundTestNegative() {
        @Nullable final String token = authService.login("ABYRVALG", "PASS");
    }

    @Test(expected = AuthenticationException.class)
    public void loginUserLockedTestNegative() {
        user.setLocked(true);
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
    }

    @Test(expected = AuthenticationException.class)
    public void loginWrongPasswordTestNegative() {
        @Nullable final String token = authService.login(user.getLogin(), "PASS");
    }

    @Test
    public void logoutTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final Session validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            boolean isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertTrue(isSessionExists);
            authService.logout(token);
            isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertFalse(isSessionExists);
        }
    }

    @Test(expected = AccessDeniedException.class)
    public void logoutTokenNullTestNegative() {
        authService.logout(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void logoutWrongTokenTestNegative() {
        @NotNull final Session validSession = authService.validateToken("123456");
    }

    @Test
    public void validateTokenTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final Session validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            boolean isSessionExists = sessionService.existsById(validSession.getId());
            Assert.assertTrue(isSessionExists);
        }
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTokenTokenNullTestNegative() {
        @NotNull final Session validSession = authService.validateToken(null);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTokenWrongTokenTestNegative() {
        @NotNull final Session validSession = authService.validateToken("123456");
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTokenExpiredSessionTestNegative() throws JsonProcessingException {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final Session validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -30);
        @NotNull final Date dateBefore = cal.getTime();
        validSession.setDate(dateBefore);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String tokenForMapper = objectMapper.writeValueAsString(validSession);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String invalidToken =  CryptUtil.encrypt(tokenForMapper, sessionKey);
        @NotNull final Session invalidSession = authService.validateToken(invalidToken);
    }

    @Test(expected = AccessDeniedException.class)
    public void validateTokenSessionNotExistTestNegative() {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final Session validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final String sessionId = validSession.getId();
        sessionService.removeById(sessionId);
        @NotNull final Session invalidSession = authService.validateToken(token);
    }

    @Test
    public void invalidateTest() {
        Assert.assertTrue(sessionService.getSize() > 0);
        @NotNull final List<Session> sessionList = sessionService.findAll();
        for (@NotNull final Session session : sessionList) {
            authService.invalidate(session);
        }
        Assert.assertEquals(0, sessionService.getSize());
    }

    @Test
    public void InvalidateNullTest() {
        int expectedNumberOfEntries = sessionService.getSize();
        authService.invalidate(null);
        int actualNumberOfEntries = sessionService.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void registryTest() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final User user = authService.registry(login, password, email);
        Assert.assertNotNull(user);
        @Nullable final User newUser = userService.findOneById(user.getId());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
        @Nullable final String token = authService.login(newUser.getLogin(), "PASS");
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
    }

}