package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.UserNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

public class UserServiceTest {

    @NotNull
    private IUserService userService;

    @NotNull
    private IAuthService authService;

    @NotNull
    private List<User> userList;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        userService = new UserService(propertyService, userRepository, projectTaskService);
        @NotNull final ISessionService sessionService = new SessionService(sessionRepository);
        authService = new AuthService(propertyService, userService, sessionService);
        @NotNull final User test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        @NotNull final User admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        admin.setEmail("ADMIN@ADMIN");
        @NotNull final User user = userService.create("USER", "USER", Role.ADMIN);
        user.setEmail("USER@USER");
        userList = new ArrayList<>();
        userList.addAll(userService.findAll());
    }

    @Test
    public void AddTest() {
        int expectedNumberOfEntries = userService.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("TEST_LOGIN");
        user.setFirstName("TEST_FIRSTNAME");
        user.setLastName("TEST_LASTNAME");
        user.setEmail("TEST@TEST");
        user.setRole(Role.USUAL);
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void AddCollectionTest() {
        int expectedNumberOfEntries = userService.getSize() + 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST1_LOGIN");
        firstUser.setFirstName("TEST1_FIRSTNAME");
        firstUser.setLastName("TEST1_LASTNAME");
        firstUser.setEmail("TEST1@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST2_LOGIN");
        firstUser.setFirstName("TEST2_FIRSTNAME");
        firstUser.setLastName("TEST2_LASTNAME");
        firstUser.setEmail("TEST2@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.add(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void createTest() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final User user = userService.create(login, password);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    public void createLoginEmptyTestNegative() {
        userService.create("", "PASS");
    }

    @Test(expected = LoginExistsException.class)
    public void createLoginExistsTestNegative() {
        userService.create("TEST", "PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    public void createPasswordEmptyTestNegative() {
        userService.create("LOGIN", "");
    }

    @Test
    public void createWithEmailTest() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final User user = userService.create(login, password, email);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
    }

    @Test(expected = LoginEmptyException.class)
    public void createWithEmailLoginEmptyTestNegative() {
        userService.create("", "PASS", "EMAIL");
    }

    @Test(expected = LoginExistsException.class)
    public void createWithEmailLoginExistsTestNegative() {
        userService.create("TEST", "PASS", "EMAIL");
    }

    @Test(expected = PasswordEmptyException.class)
    public void createWithEmailPasswordEmptyTestNegative() {
        userService.create("LOGIN", "", "EMAIL");
    }

    @Test(expected = EmailExistsException.class)
    public void createWithEmailEmailExistsTestNegative() {
        userService.create("LOGIN", "PASS", "ppanasyuk@t1-consulting.ru");
    }

    @Test
    public void createWithRole() {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final Role role = Role.USUAL;
        @NotNull final User user = userService.create(login, password, role);
        @NotNull final String userId = user.getId();
        @Nullable final User newUser = userService.findOneById(userId);
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(role, newUser.getRole());
    }

    @Test(expected = LoginEmptyException.class)
    public void createWithRoleLoginEmptyTestNegative() {
        userService.create("", "PASS", Role.USUAL);
    }

    @Test(expected = LoginExistsException.class)
    public void createWithRoleLoginExistsTestNegative() {
        userService.create("TEST", "PASS", Role.USUAL);
    }

    @Test(expected = PasswordEmptyException.class)
    public void createWithRolePasswordEmptyTestNegative() {
        userService.create("LOGIN", "", Role.USUAL);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(userService.getSize() > 0);
        userService.clear();
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<User> userList = new ArrayList<>();
        @NotNull final User firstUser = new User();
        firstUser.setLogin("TEST1_LOGIN");
        firstUser.setFirstName("TEST1_FIRSTNAME");
        firstUser.setLastName("TEST1_LASTNAME");
        firstUser.setEmail("TEST1@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(firstUser);
        @NotNull final User secondUser = new User();
        firstUser.setLogin("TEST_LOGIN");
        firstUser.setFirstName("TEST_FIRSTNAME");
        firstUser.setLastName("TEST_LASTNAME");
        firstUser.setEmail("TEST@TEST");
        firstUser.setRole(Role.USUAL);
        userList.add(secondUser);
        userService.set(userList);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void existByIdTrueTest() {
        for (@NotNull final User user : userList) {
            final boolean isExist = userService.existsById(user.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void existByIdFalseTest() {
        final boolean isExist = userService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<User> users = userService.findAll();
        Assert.assertEquals(userList, users);
    }

    @Test
    public void findByLoginTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final  User foundUser = userService.findByLogin(user.getLogin());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    public void findByLoginEmptyTestNegative() {
        @Nullable final User user = userService.findByLogin("");
    }

    @Test(expected = LoginEmptyException.class)
    public void findByLoginNullTestNegative() {
        @Nullable final User user = userService.findByLogin(null);
    }

    @Test
    public void findByEmailTest() {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final User user : userList) {
            @Nullable final  User foundUser = userService.findByEmail(user.getEmail());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void findOneByIdTest() {
        @Nullable User foundUser;
        for (@NotNull final User user : userList) {
            foundUser = userService.findOneById(user.getId());
            Assert.assertNotNull(foundUser);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final User foundUser = userService.findOneById(null);
        Assert.assertNull(foundUser);
    }

    @Test
    public void findOneByIdEmptyTest() {
        @Nullable final User foundUser = userService.findOneById("");
        Assert.assertNull(foundUser);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < userList.size(); i++) {
            @Nullable final User user = userService.findOneByIndex(i);
            Assert.assertNotNull(user);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexIndexIncorrectNegative() {
        int index = userService.getSize() + 1;
        @Nullable final User user = userService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexNullIndexIncorrectNegative() {
        @Nullable final User user = userService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexMinusIndexIncorrectNegative() {
        @Nullable final User user = userService.findOneByIndex(-1);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = userList.size();
        int actualSize = userService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void isLoginExistTrueTest() {
        boolean isExist = userService.isLoginExist("ADMIN");
        Assert.assertTrue(isExist);
    }

    @Test
    public void isLoginExistFalseTest() {
        boolean isExist = userService.isLoginExist("ABYRVALG");
        Assert.assertFalse(isExist);
    }

    @Test
    public void isLoginExistNullFalseTest() {
        boolean isExist = userService.isLoginExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void isLoginExistEmptyFalseTest() {
        boolean isExist = userService.isLoginExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void isEmailExistTrueTest() {
        boolean isExist = userService.isEmailExist("ppanasyuk@t1-consulting.ru");
        Assert.assertTrue(isExist);
    }

    @Test
    public void isEmailExistFalseTest() {
        boolean isExist = userService.isEmailExist("QWERTY@QWERTY");
        Assert.assertFalse(isExist);
    }

    @Test
    public void isEmailExistNullFalseTest() {
        boolean isExist = userService.isEmailExist(null);
        Assert.assertFalse(isExist);
    }

    @Test
    public void isEmailExistEmptyFalseTest() {
        boolean isExist = userService.isEmailExist("");
        Assert.assertFalse(isExist);
    }

    @Test
    public void lockUserByLoginTest() {
        userService.lockUserByLogin("TEST");
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        Assert.assertEquals(true, user.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void lockUserByLoginLoginNullTestNegative() {
        userService.lockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    public void lockUserByLoginLoginEmptyTestNegative() {
        userService.lockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void lockUserByLoginUserNotFoundTestNegative() {
        userService.lockUserByLogin("123");
    }

    @Test
    public void removeByLoginTest() {
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByLogin("TEST");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByLogin("TEST");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = LoginEmptyException.class)
    public void removeByLoginLoginNullTestNegative() {
        userService.removeByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    public void removeByLoginLoginEmptyTestNegative() {
        userService.removeByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void removeByLoginUserNotFoundTestNegative() {
        userService.removeByLogin("123");
    }

    @Test
    public void removeByEmailTest() {
        @Nullable final User user = userService.findByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNotNull(user);
        @Nullable User deletedUser = userService.removeByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNotNull(deletedUser);
        deletedUser = userService.findByEmail("ppanasyuk@t1-consulting.ru");
        Assert.assertNull(deletedUser);
    }

    @Test(expected = EmailEmptyException.class)
    public void removeByEmailEmailNullTestNegative() {
        userService.removeByEmail(null);
    }

    @Test(expected = EmailEmptyException.class)
    public void removeByEmailEmailEmptyTestNegative() {
        userService.removeByEmail("");
    }

    @Test(expected = UserNotFoundException.class)
    public void removeByEmailUserNotFoundTestNegative() {
        userService.removeByEmail("123");
    }

    @Test
    public void setPasswordTest() {
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        @Nullable final User updatedUser = userService.setPassword(user.getId(), "NEW_PASS");
        Assert.assertNotNull(updatedUser);
        @Nullable final String token = authService.login("TEST", "NEW_PASS");
        Assert.assertNotNull(token);
    }

    @Test(expected = IdEmptyException.class)
    public void setPasswordIdNullTestNegative() {
        userService.setPassword(null, "NEW_PASS");
    }

    @Test(expected = IdEmptyException.class)
    public void setPasswordIdEmptyTestNegative() {
        userService.setPassword("", "NEW_PASS");
    }

    @Test(expected = PasswordEmptyException.class)
    public void setPasswordPasswordNullTestNegative() {
        userService.setPassword("ID", null);
    }

    @Test(expected = PasswordEmptyException.class)
    public void setPasswordPasswordEmptyTestNegative() {
        userService.setPassword("ID", "");
    }

    @Test(expected = UserNotFoundException.class)
    public void setPasswordUserNotFoundTestNegative() {
        userService.setPassword("USER123", "NEW_PASS");
    }

    @Test
    public void removeTest() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.remove(user);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNotFoundTestNegative() {
        @NotNull final User user = new User();
        userService.remove(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNullNotFoundTestNegative() {
        @Nullable final User deletedUser = userService.remove(null);
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final User user : userList) {
            @NotNull final String userId = user.getId();
            @Nullable final User deletedUser = userService.removeById(userId);
            Assert.assertNotNull(deletedUser);
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdNullTestNegative() {
        userService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdEmptyTestNegative() {
        userService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIdEntityNotFoundTestNegative() {
        userService.removeById("123321");
    }

    @Test
    public void removeByIndexTest() {
        int index = userList.size();
        while (index > 0) {
            @Nullable final User deletedUser = userService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedUser);
            @NotNull final String userId = deletedUser.getId();
            @Nullable final User deletedUserInRepository = userService.findOneById(userId);
            Assert.assertNull(deletedUserInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIndexIncorrectTestNegative() {
        int index = userList.size() + 1;
        userService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexNullIndexIncorrectTestNegative() {
        userService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexMinusIndexIncorrectTestNegative() {
        userService.removeByIndex(-1);
    }

    @Test
    public void unlockUserByLoginTest() {
        userService.lockUserByLogin("TEST");
        @Nullable final User lockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(lockedUser);
        Assert.assertEquals(true, lockedUser.getLocked());
        userService.unlockUserByLogin("TEST");
        @Nullable final User unlockedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(unlockedUser);
        Assert.assertEquals(false, unlockedUser.getLocked());
    }

    @Test(expected = LoginEmptyException.class)
    public void unlockUserByLoginLoginNullTestNegative() {
        userService.unlockUserByLogin(null);
    }

    @Test(expected = LoginEmptyException.class)
    public void unlockUserByLoginLoginEmptyTestNegative() {
        userService.unlockUserByLogin("");
    }

    @Test(expected = UserNotFoundException.class)
    public void unlockUserByLoginUserNotFoundTestNegative() {
        userService.unlockUserByLogin("123");
    }

    @Test
    public void updateUser() {
        @NotNull final String firstName = "NEW_FIRST_NAME";
        @NotNull final String lastName = "NEW_LAST_NAME";
        @NotNull final String middleName = "NEW_MIDDLE_NAME";
        @Nullable final User user = userService.findByLogin("TEST");
        Assert.assertNotNull(user);
        userService.updateUser(user.getId(), firstName, lastName, middleName);
        @Nullable final User updatedUser = userService.findByLogin("TEST");
        Assert.assertNotNull(updatedUser);
        Assert.assertEquals(lastName, updatedUser.getLastName());
        Assert.assertEquals(firstName, updatedUser.getFirstName());
        Assert.assertEquals(middleName, updatedUser.getMiddleName());
    }

    @Test(expected = IdEmptyException.class)
    public void updateUserLoginNullTestNegative() {
        userService.updateUser(null, "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = IdEmptyException.class)
    public void updateUserLoginEmptyTestNegative() {
        userService.updateUser("", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

    @Test(expected = UserNotFoundException.class)
    public void updateUserUserNotFoundTestNegative() {
        userService.updateUser("123321", "FIRST_NAME", "LAST_NAME", "MIDDLE_NAME");
    }

}