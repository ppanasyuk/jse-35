package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TaskRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String PROJECT_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Task> taskList;

    @NotNull
    private ITaskRepository taskRepository;

    @Before
    public void initRepository() {
        taskList = new ArrayList<>();
        taskRepository = new TaskRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task " + i);
            task.setDescription("Description " + i);
            if (i < 5) {
                task.setUserId(USER_1_ID);
                task.setProjectId(PROJECT_1_ID);
            } else {
                task.setUserId(USER_2_ID);
                task.setProjectId(PROJECT_2_ID);
            }
            taskList.add(task);
            taskRepository.add(task);
        }
    }

    @Test
    public void addTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final String taskName = "Task Name";
        @NotNull final String taskDescription = "Task Description";
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setDescription(taskDescription);
        taskRepository.add(USER_1_ID, task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
        @Nullable final Task createdTask = taskRepository.findOneByIndex(taskRepository.getSize() - 1);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(USER_1_ID, createdTask.getUserId());
        Assert.assertEquals(taskName, createdTask.getName());
        Assert.assertEquals(taskDescription, createdTask.getDescription());
    }

    @Test
    public void addNullTest() {
        @Nullable final Task createdTask = taskRepository.add(USER_1_ID, null);
        Assert.assertNull(createdTask);
    }

    @Test
    public void addAllTest() {
        int expectedNumberOfEntries = taskRepository.getSize() + 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "First Task Name";
        @NotNull final String firstTaskDescription = "Task Description";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Second Task Name";
        @NotNull final String secondTaskDescription = "Task Description";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.add(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        taskRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_1_ID));
    }

    @Test
    public void existByIdTrueTest() {
        boolean expectedResult = true;
        @Nullable final Task task = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertEquals(expectedResult, taskRepository.existsById(taskId));
    }

    @Test
    public void existByIdFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, taskRepository.existsById("qwerty"));
    }

    @Test
    public void existByIdForUserTrueTest() {
        boolean expectedResult = true;
        @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, 0);
        Assert.assertNotNull(task);
        @Nullable final String taskId = task.getId();
        Assert.assertEquals(expectedResult, taskRepository.existsById(USER_1_ID, taskId));
    }

    @Test
    public void existByIdForUserFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, taskRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void findAllByProjectIdTest() {
        @NotNull List<Task> taskListForProject = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT_1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_1_ID, PROJECT_1_ID);
        Assert.assertEquals(taskListForProject, tasks);
    }

    @Test
    public void findAllWithComparatorTest() {
        int expectedNumberOfEntries = taskRepository.getSize();
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(expectedNumberOfEntries, tasks.size());
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(expectedNumberOfEntries, tasks.size());
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(comparator);
        Assert.assertEquals(taskList, tasks);
    }

    @Test
    public void findAllForUserTest() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Task> tasks = taskRepository.findAll(USER_1_ID);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    public void findAllWithComparatorForUser() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .filter(m -> PROJECT_1_ID.equals(m.getProjectId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Task> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Task> tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
        comparator = Sort.BY_CREATED.getComparator();
        tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
        comparator = Sort.BY_STATUS.getComparator();
        tasks = taskRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(taskListForUser, tasks);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable Task task;
        for (int i = 0; i < taskList.size(); i++) {
            task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final Task foundTask = taskRepository.findOneById("qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    public void findOneByIdForUserTest() {
        @Nullable Task task;
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            task = taskRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task foundTask = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNotNull(foundTask);
        }
    }

    @Test
    public void findOneByIdNullForUserTest() {
        @Nullable final Task foundTask = taskRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundTask);
        @Nullable final Task foundTaskNull = taskRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundTaskNull);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < taskList.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void findOneByIndexNullTest() {
        @Nullable final Task task = taskRepository.findOneByIndex(null);
        Assert.assertNull(task);
    }

    @Test
    public void findOneByIndexForUserTest() {
        @NotNull List<Task> taskListForUser = taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < taskListForUser.size(); i++) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(task);
        }
    }

    @Test
    public void findOneByIndexNullForUserText() {
        @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(task);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = taskList.size();
        int actualSize = taskRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void getSizeForUserTest() {
        int expectedSize = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = taskRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void removeAllNullTest() {
        int expectedNumberOfEntries = taskRepository.getSize();
        taskRepository.removeAll(null);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void removeTest() {
        @Nullable final Task task = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(task);
        @NotNull final String taskId = task.getId();
        @Nullable final Task deletedTask = taskRepository.remove(task);
        Assert.assertNotNull(deletedTask);
        @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
        Assert.assertNull(deletedTaskInRepository);
    }

    @Test
    public void removeNullTest() {
        @Nullable final Task task = taskRepository.remove(null);
        Assert.assertNull(task);
    }

    @Test
    public void removeByIdTest() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task task = taskRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullTest() {
        @Nullable final Task deletedTask = taskRepository.removeById("qwerty");
        Assert.assertNull(deletedTask);
        @Nullable final Task deletedTaskNull = taskRepository.removeById(null);
        Assert.assertNull(deletedTaskNull);
    }

    @Test
    public void removeByIdForUserTest() {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task task = taskRepository.findOneByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(task);
            @NotNull final String taskId = task.getId();
            @Nullable final Task deletedTask = taskRepository.removeById(USER_1_ID, taskId);
            Assert.assertNotNull(deletedTask);
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullForUserTest() {
        @Nullable final Task deletedTask = taskRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedTask);
        @Nullable final Task deletedTaskNull = taskRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedTaskNull);
    }

    @Test
    public void removeByIndexTest() {
        int index = taskList.size();
        while (index > 0) {
            @Nullable final Task deletedTask = taskRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullTest() {
        @Nullable final Task deletedTask = taskRepository.removeByIndex(null);
        Assert.assertNull(deletedTask);
    }

    @Test
    public void removeByIndexForUserTest() {
        int index = (int) taskList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Task deletedTask = taskRepository.removeByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(deletedTask);
            @NotNull final String taskId = deletedTask.getId();
            @Nullable final Task deletedTaskInRepository = taskRepository.findOneById(USER_1_ID, taskId);
            Assert.assertNull(deletedTaskInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullForUserTest() {
        @Nullable final Task deletedTask = taskRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedTask);
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String firstTaskName = "First Task Name";
        @NotNull final String firstTaskDescription = "Task Description";
        @NotNull final Task firstTask = new Task();
        firstTask.setName(firstTaskName);
        firstTask.setDescription(firstTaskDescription);
        tasks.add(firstTask);
        @NotNull final String secondTaskName = "Second Task Name";
        @NotNull final String secondTaskDescription = "Task Description";
        @NotNull final Task secondTask = new Task();
        secondTask.setName(secondTaskName);
        secondTask.setDescription(secondTaskDescription);
        tasks.add(secondTask);
        @NotNull final Collection<Task> addedTasks = taskRepository.set(tasks);
        Assert.assertTrue(addedTasks.size() > 0);
        int actualNumberOfEntries = taskRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}