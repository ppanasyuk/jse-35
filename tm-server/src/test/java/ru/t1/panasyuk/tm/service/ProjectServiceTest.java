package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectServiceTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(projectRepository);
        @NotNull final ITaskService taskService = new TaskService(taskRepository);
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        projectList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = projectService.create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = taskService.create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        @NotNull final Task task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        @NotNull final Task task4 = taskService.create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @Test
    public void AddTest() {
        int expectedNumberOfEntries = projectService.getSize() + 1;
        @NotNull final Project project = new Project();
        project.setUserId("45");
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void AddForUserTest() {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final Project project = new Project();
        project.setUserId(test.getId());
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(test.getId(), project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    public void AddNullForUserTest() {
        int expectedNumberOfEntries = projectService.getSize(test.getId());
        @Nullable final Project project = projectService.add(test.getId(), null);
        Assert.assertNull(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    public void AddCollectionTest() {
        int expectedNumberOfEntries = projectService.getSize() + 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project firstProject = new Project();
        firstProject.setUserId("45");
        firstProject.setName("Test Add 1");
        firstProject.setDescription("Test Add 2");
        projectList.add(firstProject);
        @NotNull final Project secondProject = new Project();
        secondProject.setUserId("45");
        secondProject.setName("Test Add 3");
        secondProject.setDescription("Test Add 4");
        projectList.add(secondProject);
        projectService.add(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void changeProjectStatusByIdTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), projectId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void changeProjectStatusByIdProjectIdEmptyTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectIdEmptyException.class)
    public void changeProjectStatusByIdNullProjectIdEmptyTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    public void changeProjectStatusByIdStatusIncorrectTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "123", null);
    }

    @Test(expected = ProjectNotFoundException.class)
    public void changeProjectStatusByIdProjectNotFoundTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "123", Status.IN_PROGRESS);
    }

    @Test
    public void changeProjectStatusByIndexTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final Project project = projects.get(i);
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), i, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void changeProjectStatusByIndexIndexNullTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    public void changeProjectStatusByIndexIndexMinusTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), -1, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    public void changeProjectStatusByIIndexStatusIncorrectTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), 0, null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void changeProjectStatusByIndexIndexIncorrectTestNegative() {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), 100, Status.IN_PROGRESS);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize() > 0);
        projectService.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize(test.getId()) > 0);
        projectService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    public void createTest() {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final String name = "Project name";
        @NotNull final String description = "Project Description";
        @Nullable Project createdProject = projectService.create(test.getId(), name, description);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(test.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(description, createdProject.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    public void createByNameTest() {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final String name = "Project name";
        @Nullable Project createdProject = projectService.create(test.getId(), name);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(test.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test(expected = NameEmptyException.class)
    public void createNameEmptyTestNegative() {
        projectService.create(test.getId(), "", "description");
    }

    @Test(expected = NameEmptyException.class)
    public void createNullNameEmptyTestNegative() {
        projectService.create(test.getId(), null, "description");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void createDescriptionEmptyTestNegative() {
        projectService.create(test.getId(), "name", "");
    }

    @Test(expected = DescriptionEmptyException.class)
    public void createNullDescriptionEmptyTestNegative() {
        projectService.create(test.getId(), "name", null);
    }

    @Test(expected = NameEmptyException.class)
    public void createByNameNameEmptyTestNegative() {
        projectService.create(test.getId(), "");
    }

    @Test(expected = NameEmptyException.class)
    public void createByNameNullNameEmptyTestNegative() {
        projectService.create(test.getId(), null);
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Project> projectList = new ArrayList<>();
        @NotNull final Project firstProject = new Project();
        firstProject.setUserId("45");
        firstProject.setName("Test Add 1");
        firstProject.setDescription("Test Add 2");
        projectList.add(firstProject);
        @NotNull final Project secondProject = new Project();
        secondProject.setUserId("45");
        secondProject.setName("Test Add 3");
        secondProject.setDescription("Test Add 4");
        projectList.add(secondProject);
        projectService.set(projectList);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void existByIdTrueTest() {
        for (@NotNull final Project project : projectList) {
            final boolean isExist = projectService.existsById(project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void existByIdFalseTest() {
        final boolean isExist = projectService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void existByIdTrueForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            final boolean isExist = projectService.existsById(test.getId(), project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void existByIdFalseUserTest() {
        final boolean isExist = projectService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Project> projects = projectService.findAll();
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findAllForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void findAllWithComparatorTest() {
        @Nullable Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = null;
        projects = projectService.findAll(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findAllWithComparatorForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @Nullable Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectService.findAll(test.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(test.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(test.getId(), comparator);
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void findAllWithSortTest() {
        @NotNull List<Project> projects = projectService.findAll(Sort.BY_NAME);
        Assert.assertEquals(projectList, projects);
        projects = projectService.findAll(Sort.BY_CREATED);
        Assert.assertEquals(projectList, projects);
        projects = projectService.findAll(Sort.BY_STATUS);
        Assert.assertEquals(projectList, projects);
        @Nullable final Sort sort = null;
        projects = projectService.findAll(sort);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findAllWithSortForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull List<Project> projects = projectService.findAll(test.getId(), Sort.BY_NAME);
        Assert.assertEquals(projectsForTestUser, projects);
        projects = projectService.findAll(test.getId(), Sort.BY_CREATED);
        Assert.assertEquals(projectsForTestUser, projects);
        projects = projectService.findAll(test.getId(), Sort.BY_STATUS);
        Assert.assertEquals(projectsForTestUser, projects);
        @Nullable final Sort sort = null;
        projects = projectService.findAll(test.getId(), sort);
        Assert.assertEquals(projectsForTestUser, projects);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable Project foundProject;
        for (@NotNull final Project project : projectList) {
            foundProject = projectService.findOneById(project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void findOneByIdForUserTest() {
        @Nullable Project foundProject;
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            foundProject = projectService.findOneById(test.getId(), project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final Project foundProject = projectService.findOneById(null);
        Assert.assertNull(foundProject);
    }

    @Test
    public void findOneByIdEmptyTest() {
        @Nullable final Project foundProject = projectService.findOneById("");
        Assert.assertNull(foundProject);
    }

    @Test
    public void findOneByIdNullForUserTest() {
        @Nullable final Project foundProject = projectService.findOneById(test.getId(), null);
        Assert.assertNull(foundProject);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void findOneByIndexForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < projectsForTestUser.size(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserIndexIncorrectNegative() {
        int index = projectService.getSize(test.getId()) + 1;
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserNullIndexIncorrectNegative() {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserMinusIndexIncorrectNegative() {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexIndexIncorrectNegative() {
        int index = projectService.getSize() + 1;
        @Nullable final Project project = projectService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexNullIndexIncorrectNegative() {
        @Nullable final Project project = projectService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexMinusIndexIncorrectNegative() {
        @Nullable final Project project = projectService.findOneByIndex(-1);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = projectList.size();
        int actualSize = projectService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void getSizeForUserTest() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = projectService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void removeTest() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.remove(project);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNotFoundTestNegative() {
        @NotNull final Project project = new Project();
        projectService.remove(project);
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNullNotFoundTestNegative() {
        projectService.remove(null);
    }

    @Test
    public void removeAllTest() {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        projectService.removeAll(projectsForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final Project project : projectList) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.removeById(projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test
    public void removeByIdForUserTest() {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.removeById(test.getId(), projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(test.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdNullTestNegative() {
        projectService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdEmptyTestNegative() {
        projectService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIdEntityNotFoundTestNegative() {
        projectService.removeById("123321");
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdForUserIdNullTestNegative() {
        projectService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdForUserIdEmptyTestNegative() {
        projectService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIdForUserEntityNotFoundTestNegative() {
        projectService.removeById(test.getId(), "123321");
    }

    @Test
    public void removeByIndexTest() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project deletedProject = projectService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexForUserTest() {
        int index = (int) projectList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectService.removeByIndex(test.getId(), index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(test.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIndexIncorrectTestNegative() {
        int index = projectList.size() + 1;
        projectService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexNullIndexIncorrectTestNegative() {
        projectService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexMinusIndexIncorrectTestNegative() {
        projectService.removeByIndex(-1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexForUserIndexIncorrectTestNegative() {
        int index = projectList.size() + 1;
        projectService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexNullForUserIndexIncorrectTestNegative() {
        projectService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() {
        projectService.removeByIndex(test.getId(), -1);
    }

    @Test
    public void updateByIdTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateById(test.getId(), projectId, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    public void updateByIndexTest() {
        @NotNull final List<Project> projects = projectService.findAll(test.getId());
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateByIndex(test.getId(), index, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test(expected = IdEmptyException.class)
    public void UpdateByIdIdEmptyTestNegative() {
        projectService.updateById(test.getId(), "", "name", "description");
    }

    @Test(expected = IdEmptyException.class)
    public void UpdateByIdNullIdEmptyTestNegative() {
        projectService.updateById(test.getId(), null, "name", "description");
    }

    @Test(expected = NameEmptyException.class)
    public void UpdateByIdNameEmptyTestNegative() {
        projectService.updateById(test.getId(), "id", "", "description");
    }

    @Test(expected = NameEmptyException.class)
    public void UpdateByIdNullNameEmptyTestNegative() {
        projectService.updateById(test.getId(), "id", null, "description");
    }

    @Test(expected = ProjectNotFoundException.class)
    public void UpdateByIdProjectNotFoundTestNegative() {
        projectService.updateById(test.getId(), "123", "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    public void UpdateByIndexIndexNullTestNegative() {
        projectService.updateByIndex(test.getId(), null, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    public void UpdateByIndexMinusTestNegative() {
        projectService.updateByIndex(test.getId(), -1, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    public void UpdateByIndexIndexIncorrectTestNegative() {
        projectService.updateByIndex(test.getId(), 100, "", "description");
    }

    @Test(expected = NameEmptyException.class)
    public void UpdateByIndexNullNameEmptyTestNegative() {
        projectService.updateByIndex(test.getId(), 0, null, "description");
    }

    @Test(expected = NameEmptyException.class)
    public void UpdateByIndexNameEmptyTestNegative() {
        projectService.updateByIndex(test.getId(), 0, "", "description");
    }

}