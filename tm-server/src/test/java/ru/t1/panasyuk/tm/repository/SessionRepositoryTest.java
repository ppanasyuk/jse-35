package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SessionRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        sessionRepository = new SessionRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i < 5) session.setUserId(USER_1_ID);
            else session.setUserId(USER_2_ID);
            sessionList.add(session);
            sessionRepository.add(session);
        }
    }

    @Test
    public void addTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        sessionRepository.add(USER_1_ID, session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session createdSession = sessionRepository.findOneByIndex(sessionRepository.getSize() - 1);
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(USER_1_ID, createdSession.getUserId());
    }

    @Test
    public void addNullTest() {
        @Nullable final Session createdSession = sessionRepository.add(USER_1_ID, null);
        Assert.assertNull(createdSession);
    }

    @Test
    public void addAllTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.add(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
    }

    @Test
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(USER_1_ID));
    }

    @Test
    public void existByIdTrueTest() {
        boolean expectedResult = true;
        @Nullable final Session session = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertEquals(expectedResult, sessionRepository.existsById(sessionId));
    }

    @Test
    public void existByIdFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, sessionRepository.existsById("qwerty"));
    }

    @Test
    public void existByIdForUserTrueTest() {
        boolean expectedResult = true;
        @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, 0);
        Assert.assertNotNull(session);
        @Nullable final String sessionId = session.getId();
        Assert.assertEquals(expectedResult, sessionRepository.existsById(USER_1_ID, sessionId));
    }

    @Test
    public void existByIdForUserFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, sessionRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Session> sessions = sessionRepository.findAll();
        Assert.assertEquals(sessionList, sessions);
    }

    @Test
    public void findAllForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Session> sessions = sessionRepository.findAll(USER_1_ID);
        Assert.assertEquals(sessionListForUser, sessions);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable Session session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById("qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    public void findOneByIdForUserTest() {
        @Nullable Session session;
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionListForUser.size(); i++) {
            session = sessionRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void findOneByIdNullForUserTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < sessionList.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void findOneByIndexNullTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(null);
        Assert.assertNull(session);
    }

    @Test
    public void findOneByIndexForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionListForUser.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void findOneByIndexNullForUserText() {
        @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(session);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = sessionList.size();
        int actualSize = sessionRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void getSizeForUserTest() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = sessionRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void removeAllNullTest() {
        int expectedNumberOfEntries = sessionRepository.getSize();
        sessionRepository.removeAll(null);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void removeTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(0);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final Session deletedSession = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSession);
        @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
        Assert.assertNull(deletedSessionInRepository);
    }

    @Test
    public void removeNullTest() {
        @Nullable final Session session = sessionRepository.remove(null);
        Assert.assertNull(session);
    }

    @Test
    public void removeByIdTest() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullTest() {
        @Nullable final Session deletedSession = sessionRepository.removeById("qwerty");
        Assert.assertNull(deletedSession);
        @Nullable final Session deletedSessionNull = sessionRepository.removeById(null);
        Assert.assertNull(deletedSessionNull);
    }

    @Test
    public void removeByIdForUserTest() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session session = sessionRepository.findOneByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionRepository.removeById(USER_1_ID, sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullForUserTest() {
        @Nullable final Session deletedSession = sessionRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedSession);
        @Nullable final Session deletedSessionNull = sessionRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedSessionNull);
    }

    @Test
    public void removeByIndexTest() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullTest() {
        @Nullable final Session deletedSession = sessionRepository.removeByIndex(null);
        Assert.assertNull(deletedSession);
    }

    @Test
    public void removeByIndexForUserTest() {
        int index = (int) sessionList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionRepository.removeByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(USER_1_ID, sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullForUserTest() {
        @Nullable final Session deletedSession = sessionRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedSession);
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.set(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}