package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionServiceTest {

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionService sessionService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @NotNull
    private User user;

    @Before
    public void initService() {
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        @NotNull final IUserRepository userRepository = new UserRepository();
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository();
        @NotNull final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
        @NotNull final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);
        sessionService = new SessionService(sessionRepository);
        test = userService.create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        admin = userService.create("ADMIN", "ADMIN", Role.ADMIN);
        user = userService.create("USER", "USER", Role.ADMIN);
        @NotNull final Session session1 = new Session();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        @NotNull final Session session2 = new Session();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        @NotNull final Session session3 = new Session();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
        sessionService.add(sessionList);
    }

    @Test
    public void AddTest() {
        int expectedNumberOfEntries = sessionService.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setUserId("45");
        session.setRole(Role.USUAL);
        sessionService.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void AddForUserTest() {
        int expectedNumberOfEntries = sessionService.getSize(test.getId()) + 1;
        @NotNull final Session session = new Session();
        session.setUserId(test.getId());
        session.setRole(test.getRole());
        sessionService.add(test.getId(), session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    public void AddNullForUserTest() {
        int expectedNumberOfEntries = sessionService.getSize(test.getId());
        @Nullable final Session session = sessionService.add(test.getId(), null);
        Assert.assertNull(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    public void AddCollectionTest() {
        int expectedNumberOfEntries = sessionService.getSize() + 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId("45");
        firstSession.setRole(Role.USUAL);
        sessionList.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId("45");
        secondSession.setRole(Role.USUAL);
        sessionList.add(secondSession);
        sessionService.add(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize() > 0);
        sessionService.clear();
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(sessionService.getSize(test.getId()) > 0);
        sessionService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Session> sessionList = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setUserId("45");
        firstSession.setRole(Role.USUAL);
        sessionList.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setUserId("45");
        secondSession.setRole(Role.USUAL);
        sessionList.add(secondSession);
        sessionService.set(sessionList);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize());
    }

    @Test
    public void existByIdTrueTest() {
        for (@NotNull final Session session : sessionList) {
            final boolean isExist = sessionService.existsById(session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void existByIdFalseTest() {
        final boolean isExist = sessionService.existsById("123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void existByIdTrueForUserTest() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            final boolean isExist = sessionService.existsById(test.getId(), session.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    public void existByIdFalseUserTest() {
        final boolean isExist = sessionService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Session> sessions = sessionService.findAll();
        Assert.assertEquals(sessionList, sessions);
    }

    @Test
    public void findAllForUserTest() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Session> sessions = sessionService.findAll(test.getId());
        Assert.assertEquals(sessionsForTestUser, sessions);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable Session foundSession;
        for (@NotNull final Session session : sessionList) {
            foundSession = sessionService.findOneById(session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void findOneByIdForUserTest() {
        @Nullable Session foundSession;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            foundSession = sessionService.findOneById(test.getId(), session.getId());
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final Session foundSession = sessionService.findOneById(null);
        Assert.assertNull(foundSession);
    }

    @Test
    public void findOneByIdEmptyTest() {
        @Nullable final Session foundSession = sessionService.findOneById("");
        Assert.assertNull(foundSession);
    }

    @Test
    public void findOneByIdNullForUserTest() {
        @Nullable final Session foundSession = sessionService.findOneById(test.getId(), null);
        Assert.assertNull(foundSession);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < sessionList.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    public void findOneByIndexForUserTest() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sessionsForTestUser.size(); i++) {
            @Nullable final Session session = sessionService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserIndexIncorrectNegative() {
        int index = sessionService.getSize(test.getId()) + 1;
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserNullIndexIncorrectNegative() {
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexForUserMinusIndexIncorrectNegative() {
        @Nullable final Session session = sessionService.findOneByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexIndexIncorrectNegative() {
        int index = sessionService.getSize() + 1;
        @Nullable final Session session = sessionService.findOneByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexNullIndexIncorrectNegative() {
        @Nullable final Session session = sessionService.findOneByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void findOneByIndexMinusIndexIncorrectNegative() {
        @Nullable final Session session = sessionService.findOneByIndex(-1);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = sessionList.size();
        int actualSize = sessionService.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void getSizeForUserTest() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        int actualSize = sessionService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void removeTest() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.remove(session);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNotFoundTestNegative() {
        @NotNull final Session session = new Session();
        sessionService.remove(session);
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeEntityNullNotFoundTestNegative() {
        @Nullable final Session deletedSession = sessionService.remove(null);
    }

    @Test
    public void removeAllTest() {
        int expectedNumberOfEntries = 0;
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        sessionService.removeAll(sessionsForTestUser);
        Assert.assertEquals(expectedNumberOfEntries, sessionService.getSize(test.getId()));
    }

    @Test
    public void removeByIdTest() {
        for (@NotNull final Session session : sessionList) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test
    public void removeByIdForUserTest() {
        @NotNull final List<Session> sessionsForTestUser = sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .collect(Collectors.toList());
        for (@NotNull final Session session : sessionsForTestUser) {
            @NotNull final String sessionId = session.getId();
            @Nullable final Session deletedSession = sessionService.removeById(test.getId(), sessionId);
            Assert.assertNotNull(deletedSession);
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdNullTestNegative() {
        sessionService.removeById(null);
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdIdEmptyTestNegative() {
        sessionService.removeById("");
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIdEntityNotFoundTestNegative() {
        sessionService.removeById("123321");
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdForUserIdNullTestNegative() {
        sessionService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    public void removeByIdForUserIdEmptyTestNegative() {
        sessionService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    public void removeByIdForUserEntityNotFoundTestNegative() {
        sessionService.removeById(test.getId(), "123321");
    }

    @Test
    public void removeByIndexTest() {
        int index = sessionList.size();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexForUserTest() {
        int index = (int) sessionList
                .stream()
                .filter(m -> test.getId().equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Session deletedSession = sessionService.removeByIndex(test.getId(), index - 1);
            Assert.assertNotNull(deletedSession);
            @NotNull final String sessionId = deletedSession.getId();
            @Nullable final Session deletedSessionInRepository = sessionService.findOneById(test.getId(), sessionId);
            Assert.assertNull(deletedSessionInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexIndexIncorrectTestNegative() {
        int index = sessionList.size() + 1;
        sessionService.removeByIndex(index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexNullIndexIncorrectTestNegative() {
        sessionService.removeByIndex(null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexMinusIndexIncorrectTestNegative() {
        sessionService.removeByIndex(-1);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexForUserIndexIncorrectTestNegative() {
        int index = sessionList.size() + 1;
        sessionService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexNullForUserIndexIncorrectTestNegative() {
        sessionService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() {
        sessionService.removeByIndex(test.getId(), -1);
    }

}