package ru.t1.panasyuk.tm.service;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.repository.ProjectRepository;
import ru.t1.panasyuk.tm.repository.SessionRepository;
import ru.t1.panasyuk.tm.repository.TaskRepository;
import ru.t1.panasyuk.tm.repository.UserRepository;

import java.util.List;

public class DomainServiceTest {

    @NotNull
    private IServiceLocator serviceLocator;

    @Before
    public void initService() {
        serviceLocator = new IServiceLocator() {

            @NotNull final IProjectRepository projectRepository = new ProjectRepository();

            @NotNull final IUserRepository userRepository = new UserRepository();

            @NotNull final ITaskRepository taskRepository = new TaskRepository();

            @NotNull final ISessionRepository sessionRepository = new SessionRepository();

            @Getter
            @NotNull
            final IPropertyService propertyService = new PropertyService();

            @Getter
            @NotNull
            final IProjectService projectService = new ProjectService(projectRepository);

            @Getter
            @NotNull
            final ITaskService taskService = new TaskService(taskRepository);

            @Getter
            @NotNull
            final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

            @Getter
            @NotNull
            final IUserService userService = new UserService(propertyService, userRepository, projectTaskService);

            @Getter
            @NotNull
            final IDomainService domainService = new DomainService(this);

            @Getter
            @NotNull
            final ISessionService sessionService = new SessionService(sessionRepository);

            @Getter
            @NotNull
            final IAuthService authService = new AuthService(propertyService, userService, sessionService);

            @Getter
            @NotNull
            final ILoggerService loggerService = new LoggerService(propertyService);

        };

        @NotNull final User test = serviceLocator.getUserService().create("TEST", "TEST", "ppanasyuk@t1-consulting.ru");
        @NotNull final User admin = serviceLocator.getUserService().create("ADMIN", "ADMIN", Role.ADMIN);
        @NotNull final Project project1 = serviceLocator.getProjectService().create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = serviceLocator.getProjectService().create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = serviceLocator.getProjectService().create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = serviceLocator.getTaskService().create(test.getId(), "Task 1", "Task for project 1");
        task1.setProjectId(project1.getId());
        @NotNull final Task task2 = serviceLocator.getTaskService().create(test.getId(), "Task 2", "Task for project 1");
        task2.setProjectId(project1.getId());
        @NotNull final Task task3 = serviceLocator.getTaskService().create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProjectId(project2.getId());
        @NotNull final Task task4 = serviceLocator.getTaskService().create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProjectId(project2.getId());
    }

    @Test
    public void dataBackupTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBackup();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBackup();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataBase64Test() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBase64();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBase64();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataBinaryTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataBinary();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataBinary();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataJsonFasterXMLTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonFasterXML();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataJsonJaxBTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataJsonJaxB();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataJsonJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataXMLFasterXMLTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLFasterXML();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXMLFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataXMLJaxBTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataXMLJaxB();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataXMLJaxB();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

    @Test
    public void dataYamlFasterXMLTest() {
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        Assert.assertTrue(projects.size() > 0);
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        Assert.assertTrue(tasks.size() > 0);
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        Assert.assertTrue(users.size() > 0);
        serviceLocator.getDomainService().saveDataYamlFasterXML();
        serviceLocator.getProjectService().clear();
        serviceLocator.getTaskService().clear();
        serviceLocator.getUserService().clear();
        Assert.assertEquals(0, serviceLocator.getProjectService().getSize());
        Assert.assertEquals(0, serviceLocator.getTaskService().getSize());
        Assert.assertEquals(0, serviceLocator.getUserService().getSize());
        serviceLocator.getDomainService().loadDataYamlFasterXML();
        Assert.assertEquals(projects, serviceLocator.getProjectService().findAll());
        Assert.assertEquals(tasks, serviceLocator.getTaskService().findAll());
        Assert.assertEquals(users, serviceLocator.getUserService().findAll());
    }

}