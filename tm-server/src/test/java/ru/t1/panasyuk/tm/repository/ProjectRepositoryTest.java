package ru.t1.panasyuk.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.util.SystemUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProjectRepositoryTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final String USER_1_ID = SystemUtil.generateGuid();

    @NotNull
    private final String USER_2_ID = SystemUtil.generateGuid();

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectRepository projectRepository;

    @Before
    public void initRepository() {
        projectList = new ArrayList<>();
        projectRepository = new ProjectRepository();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project " + i);
            project.setDescription("Description " + i);
            if (i < 5) project.setUserId(USER_1_ID);
            else project.setUserId(USER_2_ID);
            projectList.add(project);
            projectRepository.add(project);
        }
    }

    @Test
    public void addTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final String projectName = "Project Name";
        @NotNull final String projectDescription = "Project Description";
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setDescription(projectDescription);
        projectRepository.add(USER_1_ID, project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
        @Nullable final Project createdProject = projectRepository.findOneByIndex(projectRepository.getSize() - 1);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(USER_1_ID, createdProject.getUserId());
        Assert.assertEquals(projectName, createdProject.getName());
        Assert.assertEquals(projectDescription, createdProject.getDescription());
    }

    @Test
    public void addNullTest() {
        @Nullable final Project createdProject = projectRepository.add(USER_1_ID, null);
        Assert.assertNull(createdProject);
    }

    @Test
    public void addAllTest() {
        int expectedNumberOfEntries = projectRepository.getSize() + 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "First Project Name";
        @NotNull final String firstProjectDescription = "Project Description";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Second Project Name";
        @NotNull final String secondProjectDescription = "Project Description";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.add(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void clearTest() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        projectRepository.clear(USER_1_ID);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_1_ID));
    }

    @Test
    public void existByIdTrueTest() {
        boolean expectedResult = true;
        @Nullable final Project project = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertEquals(expectedResult, projectRepository.existsById(projectId));
    }

    @Test
    public void existByIdFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, projectRepository.existsById("qwerty"));
    }

    @Test
    public void existByIdForUserTrueTest() {
        boolean expectedResult = true;
        @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, 0);
        Assert.assertNotNull(project);
        @Nullable final String projectId = project.getId();
        Assert.assertEquals(expectedResult, projectRepository.existsById(USER_1_ID, projectId));
    }

    @Test
    public void existByIdForUserFalseTest() {
        boolean expectedResult = false;
        Assert.assertEquals(expectedResult, projectRepository.existsById(USER_1_ID, "qwerty"));
    }

    @Test
    public void findAllTest() {
        @NotNull final List<Project> projects = projectRepository.findAll();
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findAllWithComparatorTest() {
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(projectList, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(comparator);
        Assert.assertEquals(projectList, projects);
    }

    @Test
    public void findAllForUserTest() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull final List<Project> projects = projectRepository.findAll(USER_1_ID);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    public void findAllWithComparatorForUser() {
        @NotNull final List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        @NotNull Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @NotNull List<Project> projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectRepository.findAll(USER_1_ID, comparator);
        Assert.assertEquals(projectListForUser, projects);
    }

    @Test
    public void findOneByIdTest() {
        @Nullable Project project;
        for (int i = 0; i < projectList.size(); i++) {
            project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void findOneByIdNullTest() {
        @Nullable final Project foundProject = projectRepository.findOneById("qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    public void findOneByIdForUserTest() {
        @Nullable Project project;
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < projectListForUser.size(); i++) {
            project = projectRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project foundProject = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    public void findOneByIdNullForUserTest() {
        @Nullable final Project foundProject = projectRepository.findOneById(USER_1_ID, "qwerty");
        Assert.assertNull(foundProject);
        @Nullable final Project foundProjectNull = projectRepository.findOneById(USER_1_ID, null);
        Assert.assertNull(foundProjectNull);
    }

    @Test
    public void findOneByIndexTest() {
        for (int i = 0; i < projectList.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void findOneByIndexNullTest() {
        @Nullable final Project project = projectRepository.findOneByIndex(null);
        Assert.assertNull(project);
    }

    @Test
    public void findOneByIndexForUserTest() {
        @NotNull List<Project> projectListForUser = projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .collect(Collectors.toList());
        for (int i = 0; i < projectListForUser.size(); i++) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, i);
            Assert.assertNotNull(project);
        }
    }

    @Test
    public void findOneByIndexNullForUserText() {
        @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, null);
        Assert.assertNull(project);
    }

    @Test
    public void getSizeTest() {
        int expectedSize = projectList.size();
        int actualSize = projectRepository.getSize();
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void getSizeForUserTest() {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        int actualSize = projectRepository.getSize(USER_1_ID);
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    public void removeAllNullTest() {
        int expectedNumberOfEntries = projectRepository.getSize();
        projectRepository.removeAll(null);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    public void removeTest() {
        @Nullable final Project project = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(project);
        @NotNull final String projectId = project.getId();
        @Nullable final Project deletedProject = projectRepository.remove(project);
        Assert.assertNotNull(deletedProject);
        @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
        Assert.assertNull(deletedProjectInRepository);
    }

    @Test
    public void removeNullTest() {
        @Nullable final Project project = projectRepository.remove(null);
        Assert.assertNull(project);
    }

    @Test
    public void removeByIdTest() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project project = projectRepository.findOneByIndex(index - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullTest() {
        @Nullable final Project deletedProject = projectRepository.removeById("qwerty");
        Assert.assertNull(deletedProject);
        @Nullable final Project deletedProjectNull = projectRepository.removeById(null);
        Assert.assertNull(deletedProjectNull);
    }

    @Test
    public void removeByIdForUserTest() {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project project = projectRepository.findOneByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(project);
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectRepository.removeById(USER_1_ID, projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void removeByIdNullForUserTest() {
        @Nullable final Project deletedProject = projectRepository.removeById(USER_1_ID, "qwerty");
        Assert.assertNull(deletedProject);
        @Nullable final Project deletedProjectNull = projectRepository.removeById(USER_1_ID, null);
        Assert.assertNull(deletedProjectNull);
    }

    @Test
    public void removeByIndexTest() {
        int index = projectList.size();
        while (index > 0) {
            @Nullable final Project deletedProject = projectRepository.removeByIndex(index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullTest() {
        @Nullable final Project deletedProject = projectRepository.removeByIndex(null);
        Assert.assertNull(deletedProject);
    }

    @Test
    public void removeByIndexForUserTest() {
        int index = (int) projectList
                .stream()
                .filter(m -> USER_1_ID.equals(m.getUserId()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectRepository.removeByIndex(USER_1_ID, index - 1);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectRepository.findOneById(USER_1_ID, projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test
    public void removeByIndexNullForUserTest() {
        @Nullable final Project deletedProject = projectRepository.removeByIndex(USER_1_ID, null);
        Assert.assertNull(deletedProject);
    }

    @Test
    public void setTest() {
        int expectedNumberOfEntries = 2;
        @NotNull final List<Project> projects = new ArrayList<>();
        @NotNull final String firstProjectName = "First Project Name";
        @NotNull final String firstProjectDescription = "Project Description";
        @NotNull final Project firstProject = new Project();
        firstProject.setName(firstProjectName);
        firstProject.setDescription(firstProjectDescription);
        projects.add(firstProject);
        @NotNull final String secondProjectName = "Second Project Name";
        @NotNull final String secondProjectDescription = "Project Description";
        @NotNull final Project secondProject = new Project();
        secondProject.setName(secondProjectName);
        secondProject.setDescription(secondProjectDescription);
        projects.add(secondProject);
        @NotNull final Collection<Project> addedProjects = projectRepository.set(projects);
        Assert.assertTrue(addedProjects.size() > 0);
        int actualNumberOfEntries = projectRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

}