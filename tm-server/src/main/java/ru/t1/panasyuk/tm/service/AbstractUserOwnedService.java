package ru.t1.panasyuk.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserOwnedRepository;
import ru.t1.panasyuk.tm.api.service.IUserOwnedService;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @Nullable
    @Override
    public M add(@NotNull final String userId, @Nullable final M model) {
        if (model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Override
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public M removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.removeById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        final M model = repository.removeByIndex(userId, index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@NotNull final String userId, @Nullable final Sort sort) {
        if (sort == null) return findAll(userId);
        final Comparator<M> comparator = sort.getComparator();
        return findAll(userId, comparator);
    }

}