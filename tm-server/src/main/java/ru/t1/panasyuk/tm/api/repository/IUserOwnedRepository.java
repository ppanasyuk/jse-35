package ru.t1.panasyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @Nullable
    M add(@NotNull String userId, @Nullable M model);

    void clear(@NotNull String userId);

    boolean existsById(@NotNull String userId, @Nullable String id);

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index);

    int getSize(@NotNull String userId);

    @Nullable
    M removeById(@NotNull String userId, @Nullable String id);

    @Nullable
    M removeByIndex(@NotNull String userId, @Nullable Integer index);

}