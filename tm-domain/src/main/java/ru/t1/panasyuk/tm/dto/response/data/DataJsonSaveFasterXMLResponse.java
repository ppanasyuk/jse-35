package ru.t1.panasyuk.tm.dto.response.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.panasyuk.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public final class DataJsonSaveFasterXMLResponse extends AbstractResponse {
}