package ru.t1.panasyuk.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractRequest;

@Getter
@Setter
@NoArgsConstructor
public final class UserRegistryRequest extends AbstractRequest {

    @NotNull
    private String login;

    @NotNull
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(
            @NotNull final String login,
            @NotNull final String password,
            @Nullable final String email
    ) {
        this.login = login;
        this.password = password;
        this.email = email;
    }

}